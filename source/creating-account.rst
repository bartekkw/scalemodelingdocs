Utworzenie konta
================

Konto możesz założyć na dwa sposoby:

#. podając login i hasło
#. korzystając z integracji z wybraną aplikacją internetową

Każdy ze sposobów zajmie tylko chwilę, choć druga opcja będzie umożliwiać logowanie jednym kliknięciem bez podawania loginu i hasła.

Rejestracja loginem i hasłem
-----------------------------

Rejestracja zajmuje tylko chwilę. Musisz podać dwie informacje:

* adres e-mail - który będzie jednocześnie twoją nazwa użytkownika (login)
* hasło - minimum 6 znaków

Konieczne jest również potwierdzenie zapoznania się z regulaminem serwisu oraz polityką prywatności.

Po zatwierdzeniu powyższych danych na podany adres e-mail otrzymasz wiadomość z linkiem do aktywacji konta.

.. Tip:: Po zarejestrowaniu konta warto uzupełnić swój profil przynajmniej o datę urodzenia, która jest niezbędna aby zarejestrować model w zawodach. Zazwyczaj zawody modelarskie są rozgrywane w trzech kategoriach wiekowych: młodzik, junior i senior. Na podstawie podanej daty urodzenia system umożliwi Ci wystartować w odpowiedniej kategorii wiekowej.

.. Tip:: 
Pamiętaj, że nawet po zarejestrowaniu konta możesz skorzystać z opcji integracji z jednym z serwisów: Facebook, Google, Twitter lub Microsoft (live.com). 

Rejestracja z Facebook
----------------------

Jest to bardzo szybki i wygodny sposób na założenie konta oraz późniejsze logowanie się do serwisu.

#. Kliknij przycisk "Zarejestruje przez Facebook"
#. Zaloguj się do serwisu Facebook, jeśli nie jesteś aktualnie zalogowany
#. Wyświetlony zostanie tzw. ekran zgody (ang. consent screen)
#. Po zatwierdzeniu zostaną automatycznie pobrane twoje dane, tj. zdjęcie profilowe (avatar), adres e-mail, imię, nazwisko i - w zależności od ustawień prywatności twojego konta na FB - data urodzenia
#. Możesz ew. skorygować dane 
#. Zatwierdź wymagane zgody (regulamin serwisu oraz polityka prywatności)

.. Tip:: 
Pamiętaj, że zawsze możesz dodać hasło aby zalogować się do systemu nawet w razie niedostępności systemu zewnętrznego. 


Rejestracja z Google
---------------------

Jest to bardzo szybki i wygodny sposób na założenie konta oraz późniejsze logowanie się do serwisu.

#. Kliknij przycisk "Zarejestruje przez Facebook"
#. Zaloguj się do serwisu Facebook, jeśli nie jesteś aktualnie zalogowany
#. Wyświetlony zostanie tzw. ekran zgody (ang. consent screen)
#. Po zatwierdzeniu zostaną automatycznie pobrane twoje dane, tj. zdjęcie profilowe (avatar), adres e-mail, imię, nazwisko i - w zależności od ustawień prywatności twojego konta na FB - data urodzenia
#. Możesz ew. skorygować dane 
#. Zatwierdź wymagane zgody (regulamin serwisu oraz polityka prywatności)

.. Tip:: 
Pamiętaj, że zawsze możesz dodać hasło aby zalogować się do systemu nawet w razie niedostępności systemu zewnętrznego. 

Rejestracja z Microsoft
---------------------

Jest to bardzo szybki i wygodny sposób na założenie konta oraz późniejsze logowanie się do serwisu.

#. Kliknij przycisk "Zarejestruje przez Facebook"
#. Zaloguj się do serwisu Facebook, jeśli nie jesteś aktualnie zalogowany
#. Wyświetlony zostanie tzw. ekran zgody (ang. consent screen)
#. Po zatwierdzeniu zostaną automatycznie pobrane twoje dane, tj. zdjęcie profilowe (avatar), adres e-mail, imię, nazwisko i - w zależności od ustawień prywatności twojego konta na FB - data urodzenia
#. Możesz ew. skorygować dane 
#. Zatwierdź wymagane zgody (regulamin serwisu oraz polityka prywatności)

.. Tip:: 
Pamiętaj, że zawsze możesz dodać hasło aby zalogować się do systemu nawet w razie niedostępności systemu zewnętrznego. 

Rejestracja z Twitter
---------------------

Jest to bardzo szybki i wygodny sposób na założenie konta oraz późniejsze logowanie się do serwisu.

#. Kliknij przycisk "Zarejestruje przez Facebook"
#. Zaloguj się do serwisu Facebook, jeśli nie jesteś aktualnie zalogowany
#. Wyświetlony zostanie tzw. ekran zgody (ang. consent screen)
#. Po zatwierdzeniu zostaną automatycznie pobrane twoje dane, tj. zdjęcie profilowe (avatar), adres e-mail, imię, nazwisko i - w zależności od ustawień prywatności twojego konta na FB - data urodzenia
#. Możesz ew. skorygować dane 
#. Zatwierdź wymagane zgody (regulamin serwisu oraz polityka prywatności)

.. Tip:: 
Pamiętaj, że zawsze możesz dodać hasło aby zalogować się do systemu nawet w razie niedostępności systemu zewnętrznego. 
