Rejestracja modelu
==================

Rejestracja modelu jest możliwa dopiero po uprzednim dodaniu modelu na wirtualną półkę.

Podstawowa sprawa to uzupełnienie profilu oraz dodanie modelu na wirtualna półkę.

Jeśli masz przynajmniej jeden model na półce, możesz go zarejestrować na wybrane, obsługiwane przez nasz system, wydarzenie.


Po kliknięciu "Zarejestruj model na konkurs" musisz wybrać dostępne wydarzenie i zatwierdzić zgodę na przetwarzanie danych.


Na ekranie rejestracji należy wybrać

a) profil - domyślnie właściciela konta

b) model

c) kategorię startową



Po pomyślnym zakończeniu rejestracji pojawia się informacja o numerze startowym. 


UWAGA! Możesz od razu dodać kolejny model z półki.



Od tej chwili model jest zarejestrowany.

a) Zarejestrowany model jest widoczny w menu "Modele -> Zarejestrowane"


b) W ogólnych informacjach na stronie profilowej powinna się zwiększyć ilość rezerwacji


c) Dodatkowo powinieneś otrzymać powiadomienie e-mailem

