.. ScaleModelingDocs documentation master file, created by
   sphinx-quickstart on Sat May 12 23:08:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentacja systemu SOWM
=========================

.. toctree::
   :maxdepth: 2
   :caption: Zawartość:
   
   Opis systemu <about>
   Zawodnicy <contestants>
   Rejestracja<creating-account>
   Logowanie <signing-in>
   Organizatorzy <organizers>
   

Indeks
======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
